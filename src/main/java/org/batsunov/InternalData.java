package org.batsunov;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Данные которые хранятся в нашей системе
 */
public class InternalData {
    private LocalDate date;
    private int temperature;
    private int wetness;

    public InternalData(LocalDate date, int temperature, int wetness) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
    }



    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getWetness() {
        return wetness;
    }

    public void setWetness(int wetness) {
        this.wetness = wetness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternalData that = (InternalData) o;
        return temperature == that.temperature &&
                wetness == that.wetness &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, temperature, wetness);
    }

    @Override
    public String toString() {
        return "InternalData{" +
                "date=" + date +
                ", temperature=" + temperature +
                ", wetness=" + wetness +
                '}';
    }
}
