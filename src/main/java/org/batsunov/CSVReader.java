package org.batsunov;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.batsunov.Main.ARCHIVE;
import static org.batsunov.Main.FILE;
import static org.batsunov.Main.log;

public class CSVReader {

    public void read(String csvFile) {

        String line = "";
        String cvsSplitBy = ";";

        String date = "";
        String temperature = "";
        String wetness = "";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                String[] info = line.split(cvsSplitBy);

                for (int i = 0; i < info.length; i++) {
                    if (i % 3 == 0) {
                        date = info[i];
                    } else if (i % 2 != 0) {
                        temperature = info[i];
                    } else if (i % 2 == 0) { // в этот иф входим в последний раз,то есть инф о влажности последняяя
                        wetness = info[i];
                        log.info(FILE, "Полученые данные Дата={} Температура={} Влажность={}", date, temperature, wetness);
                    }
                }
            }

        } catch (IOException e) {
            log.error(ARCHIVE, "Вылетел IOException {}", e.getStackTrace());
        }

    }

}