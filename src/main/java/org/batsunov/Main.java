package org.batsunov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static final Logger log = LoggerFactory.getLogger(InternalData.class);
    public static final Marker FILE = MarkerFactory.getMarker("FILE");
    public static final Marker ARCHIVE = MarkerFactory.getMarker("ARCHIVE");


    public static void main(String[] args) throws IOException {
        final File tempFile = File.createTempFile("data_predicate", ".csv");

        Scanner in = new Scanner(System.in);
        System.out.println("Введите директорию в какую нужно архивировировать файл ");
        String directoryForZip = in.nextLine();


        //записуем во временный файл
        Writer writer = new Writer();
        writer.writeToCSV(tempFile, dataProvider());

        //архивируем
        ArchiverFile archiver = new ArchiverFile();
        archiver.archive(tempFile, directoryForZip);

        //разархивируем
        System.out.println("Введите папку в которой будет находиться разархивированый файл ");
        String WhereUnZip = in.nextLine();
        UnZipFile unZipper = new UnZipFile();
        String dirUnZipFileCSV = unZipper.unZip(directoryForZip, new File(WhereUnZip));

        //выведем инфу в лог
        CSVReader csvReader = new CSVReader();
        csvReader.read(dirUnZipFileCSV);


    }

    private static List<InternalData> dataProvider() {
        return List.of(new InternalData(LocalDate.of(2020, 12, 4), 7, 4),
                new InternalData(LocalDate.of(2021, 5, 6), 10, 40),
                new InternalData(LocalDate.of(2019, 8, 8), 11, 70)
        );
    }

}
