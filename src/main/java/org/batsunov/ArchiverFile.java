package org.batsunov;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.batsunov.Main.*;

public class ArchiverFile {


    public void archive(File fileToZip, String directory) {

        ZipOutputStream zipOut = null;
        FileInputStream fis = null;

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(directory);
            zipOut = new ZipOutputStream(fos);
            fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
        } catch (IOException e) {
            log.error(FILE, "Словили IOException:{}", e.getStackTrace());
        } finally {
            try {
                zipOut.close();
                fis.close();
                fos.close();
            } catch (IOException e) {
                log.error(ARCHIVE, "Вылетел IOException {}", e.getStackTrace());
            }
        }
        log.debug("Архивирование успешно завершено!", fileToZip.getName());
    }
}
