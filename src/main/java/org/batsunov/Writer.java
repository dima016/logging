package org.batsunov;

import java.io.*;
import java.util.List;

import static org.batsunov.Main.FILE;
import static org.batsunov.Main.log;


public class Writer {
    private final char separator = ';';

    public void writeToCSV(File tempFile, List<InternalData> internalDataList) {

        BufferedWriter bw = null;

        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile), "UTF-8"));
            log.info(FILE, "Начато создание файла {}", tempFile);

            for (InternalData internalData : internalDataList) {
                StringBuffer oneLine = new StringBuffer();
                oneLine.append(internalData.getDate());
                oneLine.append(separator);
                oneLine.append(internalData.getTemperature());
                oneLine.append(separator);
                oneLine.append(internalData.getWetness());
                oneLine.append(separator);
                bw.write(oneLine.toString());
                bw.newLine();
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getStackTrace());
        } catch (IOException e) {
            log.error(FILE, "Словили IOException:{}", e.getStackTrace());
        } finally {
            if (bw != null) {
                try {
                    bw.flush();
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
